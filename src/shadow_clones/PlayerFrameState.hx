package shadow_clones;

class PlayerFrameState {
    public var x: Float;
    public var y: Float;
    public var dx: Float;
    public var dy: Float;
    public var curState: Int;
    public var isLooping: Bool;

    public function new(x: Float, y: Float, dx: Float, dy: Float, curState: Int, isLooping: Bool): Void {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.curState = curState;
        this.isLooping = isLooping;
    }
}

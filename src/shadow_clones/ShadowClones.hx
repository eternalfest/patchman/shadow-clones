package shadow_clones;

import etwin.Obfu;
import hf.entity.Player;
import hf.mode.GameMode;
import hf.Hf;
import merlin.IAction;
import patchman.HostModuleLoader;
import patchman.IPatch;
import patchman.Ref;
import patchman.VoidPatch;
import shadow_clones.CloneSpawner;
import shadow_clones.PlayerFrameState;
import shadow_clones.actions.SpawnShadowClones;

@:build(patchman.Build.di())
class ShadowClones {
    private static var cloneSpawner: Null<CloneSpawner> = null;

    public static function cleanupAndResetSpawner(newSpawner: Null<CloneSpawner>) {
        if (cloneSpawner != null) {
            cloneSpawner.explodeClonesAndReinit();
            cloneSpawner.destroy();
        }
        cloneSpawner = newSpawner;
    }

    private static var optionName(default, never): String = Obfu.raw("shadowclones");

    private static function isOptionEnabled(loader: HostModuleLoader): Bool {
        var run = loader.require(Obfu.raw("run")).getRun();
        var options: Array<Dynamic> = run.gameOptions;
        for (i in 0...(options.length)) {
            if (options[i] == optionName)
                return true;
        }
        return false;
    }

    @:diExport
    public var createSpawnerOnLevelEntry(default, null): IPatch;
    @:diExport
    public var destroySpawnerOnLevelExit(default, null): IPatch;
    @:diExport
    public var storePlayersState(default, null): IPatch;
    @:diExport
    public var resetPlayerSpawnerOnDeath(default, null): IPatch;
    @:diExport
    public var removeClonesOnPlayerDestroy(default, null): IPatch;

    @:diExport
    public var spawnShadowClones(default, null): IAction;

    public function new(loader: HostModuleLoader): Void {
        if (!isOptionEnabled(loader)) {
            this.createSpawnerOnLevelEntry = new VoidPatch();
        }
        else {
            this.createSpawnerOnLevelEntry = Ref.auto(GameMode.onLevelReady).after(function(hf: Hf, self: GameMode): Void {
                /* 70 instead of 40 in the original ActionScript mod due
                 * to a change of how initial wait works. */
                cleanupAndResetSpawner(new CloneSpawner(hf, self, 3, 30, 70));
            });
        }

        this.destroySpawnerOnLevelExit = Ref.auto(GameMode.clearLevel).after(function(hf: Hf, self: GameMode): Void {
            cleanupAndResetSpawner(null);
        });

        this.storePlayersState = Ref.auto(Player.update).before(function(hf: Hf, self: Player): Void {
            if (self.fl_kill || cloneSpawner == null)
                return;

            cloneSpawner.addPlayerFrameState(self.pid, new PlayerFrameState(self.x, self.y, self.dx, self.dy, self.animId, self.fl_loop));
        });

        this.resetPlayerSpawnerOnDeath = Ref.auto(Player.killHit).before(function(hf: Hf, self: Player, dx: Null<Float>): Void {
            if (!self.fl_kill && !self.fl_shield && cloneSpawner != null)
                cloneSpawner.explodePlayerClonesAndReinit(self.pid);
        });

        removeClonesOnPlayerDestroy = Ref.auto(Player.destroy).before(function(hf: Hf, self: Player): Void {
            if (cloneSpawner != null)
                cloneSpawner.removePlayer(self.pid);
        });

        this.spawnShadowClones = new SpawnShadowClones();
    }
}
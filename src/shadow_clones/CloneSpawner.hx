package shadow_clones;

import hf.entity.Player;
import hf.mode.GameMode;
import hf.Hf;
import shadow_clones.PlayerFrameState;
import shadow_clones.ShadowClone;

class CloneSpawner {
    private var nextSpawnForPlayer: Map<Int, Int>;
    private var playerClones: Map<Int, Array<ShadowClone>>;
    private var playersInformations: Map<Int, Array<PlayerFrameState>>;

    private var hf: Hf;
    private var game: GameMode;
    private var nbClones: Int;
    private var interval: Int;
    private var initialWait: Int;

    // TODO: Handle runtime add/remove of players.

    public function new(hf: Hf, game: GameMode, nbClones: Int, interval: Int, initialWait: Int): Void {
        this.nextSpawnForPlayer = new Map<Int, Int>();
        this.playerClones = new Map<Int, Array<ShadowClone>>();
        this.playersInformations = new Map<Int, Array<PlayerFrameState>>();

        this.hf = hf;
        this.game = game;
        this.nbClones = nbClones;
        this.interval = interval;
        this.initialWait = initialWait;
        for (player in game.getPlayerList())
            initForPlayer(player.pid);

        /* For automatic call to update/endUpdate, */
        this.game.addToList(hf.Data.ENTITY, (cast this));
    }

    private function initForPlayer(pid: Int): Void {
        /* "interval - initialWait" so that the first clone will
         * spawn after a duration of "initialWait". */
        nextSpawnForPlayer.set(pid, interval - initialWait);
        playerClones.set(pid, []);
        playersInformations.set(pid, []);
    }

    public function addPlayerFrameState(pid: Int, state: PlayerFrameState) {
        this.playersInformations[pid].push(state);
    }

    @:keep
    public function update(): Void {
        var players: Array<Player> = game.getPlayerList();
        var addTargetName: Bool = players.length > 1;
        for (player in players) {
            if (player.fl_kill)
                continue;

            var clones = this.playerClones[player.pid];
            for (clone in clones)
                clone.update(player, playersInformations[player.pid]);

            if (clones.length == this.nbClones)
                continue;

            var nextSpawn = this.nextSpawnForPlayer[player.pid];
            ++nextSpawn;
            if (nextSpawn >= this.interval) {
                nextSpawn = 0;
                clones.push(new ShadowClone(this.hf, this.game, addTargetName ? player.name : null));
            }
            this.nextSpawnForPlayer[player.pid] = nextSpawn;
        }
    }

    public function explodePlayerClonesAndReinit(pid: Int): Void {
        for (clone in playerClones[pid])
            clone.explode();
        initForPlayer(pid);
    }

    public function explodeClonesAndReinit(): Void {
        for (pid in playerClones.keys())
            explodePlayerClonesAndReinit(pid);
    }

    public function removePlayer(pid: Int): Void {
        for (clone in playerClones[pid])
            clone.destroy();
        nextSpawnForPlayer.remove(pid);
        playerClones.remove(pid);
        playersInformations.remove(pid);
    }

    public function destroy(): Void {
        explodeClonesAndReinit();
        this.game.removeFromList(hf.Data.ENTITY, (cast this));
    }
}
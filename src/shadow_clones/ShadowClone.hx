package shadow_clones;

import etwin.flash.filters.ColorMatrixFilter;
import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.entity.Animator;
import hf.mode.GameMode;
import hf.Hf;
import shadow_clones.PlayerFrameState;
import color_matrix.ColorMatrix;
import sky_ui.SkyUI;

// TODO: extends Player instead of storing a movie clip?
class ShadowClone {
    private static var SHADOW_CLONE_FILTER(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(200, 1.001, -1).toFilter();

    private var hf: Hf;
    private var game: GameMode;
    private var playerClip: Player;
    private var targetNameSprite: Null<MovieClip> = null;
    private var currentFrame: Int = 0;

    public function new(hf: Hf, game: GameMode, targetName: Null<String>): Void {
        this.hf = hf;
        this.game = game;
        // TODO: Better way than cast?
        this.playerClip = (cast game.depthMan.attach("hammer_player", hf.Data.DP_PLAYER));
        playerClip.filters = [SHADOW_CLONE_FILTER];
        /* Outside of screen, will be correctly placed during update. */
        if (targetName != null)
            targetNameSprite = SkyUI.CreateNormalTextSprite(hf, game, targetName, Math.NEGATIVE_INFINITY, Math.NEGATIVE_INFINITY, 100, hf.Data.DP_PLAYER, TextAlignmentCenter);
    }

    public function update(target: Player, states: Array<PlayerFrameState>): Void {
        var state = states[this.currentFrame++];
        this.playerClip.x = state.x;
        this.playerClip.y = state.y;
        if (targetNameSprite != null) {
            var targetNameSprite = (cast this.targetNameSprite);
            targetNameSprite.field._x = state.x - targetNameSprite.field._width / 2;
            targetNameSprite.field._y = state.y - 35;
        }
        this.playerClip.dx = state.dx;
        this.playerClip.dy = state.dy;
        this.playerClip.playAnim({id: state.curState, loop: state.isLooping});
        if (this.playerClip.dx < 0)
            this.playerClip.dir = -1;
        else if (this.playerClip.dx > 0)
            this.playerClip.dir = 1;

        if (Math.abs(target.x - this.playerClip.x) < 15 && Math.abs(target.y - this.playerClip.y) < 25)
            target.killHit(target.dx);
        PartialAnimatorUpdate(this.hf, this.playerClip);
        this.playerClip.endUpdate();
    }

    private static function PartialAnimatorUpdate(hf: Hf, player: Player) {
        /* "Reflect.callMethod(this.playerClip, untyped hf.entity.Animator.prototype.update, []);"
         * seems to be breaking the game. So copy paste of the interesting part (for us) of
         * "Animator.update". See output of project-phoenix, Animator.hx:163-182. */
        if (player.frame >= 0) {
            var v3 = false;
            player.frame += player.animFactor * hf.Timer.tmod;
            while (true) {
                if (!(!v3 && player.frame >= 1)) break;
                if (player.sub._currentframe == player.sub._totalframes) {
                    if (player.fl_loop) {
                        player.sub.gotoAndStop("1");
                    } else {
                        player.frame = -1;
                        player.onEndAnim(player.animId);
                        v3 = true;
                    }
                }
                if (!v3) {
                    player.sub.nextFrame();
                    --player.frame;
                }
            }
        }
    }

    public function explode(): Void {
        game.fxMan.attachExplosion(this.playerClip.x, this.playerClip.y, 40);
        destroy();
    }

    public function destroy(): Void {
        if (targetNameSprite != null)
            targetNameSprite.removeMovieClip();
        /* I'm afraid that .destroy() would confuse the game. */
        this.playerClip.removeMovieClip();
    }
}
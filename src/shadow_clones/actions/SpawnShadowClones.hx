package shadow_clones.actions;

import etwin.ds.Nil;
import etwin.Obfu;
import hf.mode.GameMode;
import hf.Hf;
import merlin.IAction;
import merlin.IActionContext;
import shadow_clones.CloneSpawner;
import shadow_clones.ShadowClones;

class SpawnShadowClones implements IAction {
    public var name(default, null): String = Obfu.raw("shadowClones");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var nbClones: Int = ctx.getOptInt(Obfu.raw("n")).or(4);
        var initialWait: Int = ctx.getOptInt(Obfu.raw("t")).or(120);
        var interval: Int = ctx.getOptInt(Obfu.raw("int")).or(40);

        var game: GameMode = ctx.getGame();
        var hf: Hf = ctx.getHf();
        ShadowClones.cleanupAndResetSpawner(new CloneSpawner(hf, game, nbClones, interval, initialWait));

        return false;
    }
}
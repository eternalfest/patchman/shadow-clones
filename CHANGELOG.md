# 1.0.0 (2023-04-06)

- **[Fix]** Compatibility with `merlin 0.17.0`.
- **[Breaking change]** Remove  `e_` from action name.

# 0.7.0 (2021-04-28)

- **[Fix]** Compatibility with `patchman@0.10`, prevent CloneSpwaner.update from being DCE-ed.

# 0.6.0 (2021-04-22)

- **[Breaking change]** Update to `patchman@0.10.4`.
- **[Internal]** Update to Yarn 2.

# 0.5.0 (2021-03-26)

- **[Fix]** Properly handle when players are dynamically removed.

# 0.4.0 (2021-02-20)

- **[Feature]** Display the name of the target when there are multiple players.

# 0.3.0 (2020-11-29)

- **[Fix]** Compatibility with merlin 0.14.0.

# 0.2.0 (2020-07-08)

- **[Fix]** Compatibility with patchman 0.9.2.

# 0.1.0 (2020-07-08)

- **[Feature]** First release.
